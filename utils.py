import winsound
import socket

import datetime
import pytz

from timebetween import is_time_between


# timezone local assume
tz = pytz.timezone('Asia/Makassar')

REMOTE_SERVER = "mib.bankmandiri.co.id"
FAILED_COUNTER = 0


def go_beep():
    frequency = 2500
    duration = 900
    winsound.Beep(frequency, duration)


def is_connected():
    try:
        # connect to the host -- tells us if the host is actually
        # reachable
        socket.create_connection((REMOTE_SERVER, 80))
        return True
    except OSError:
        pass
    return False


def is_now_time_period(start_time, end_time, nowtime):
    if start_time < end_time:
        return start_time <= nowtime <= end_time
    else:
        # Over midnight
        return start_time >= nowtime >= end_time


def is_db_maintenance():
    now = datetime.datetime.now()
    day_num = now.strftime('%w')
    if 0 < int(day_num) < 6:
        # reference :
        # https://isipulsaonline.net/news/baca/jadwal-online-offline-internet-banking-bank-bca-mandiri-bni-bri
        maintenance_start_time = datetime.time(0, 5)
        maintenance_stop_time = datetime.time(5, 0)
        return is_time_between(now.time(), maintenance_start_time, maintenance_stop_time)
    else:
        maintenance_start_time = datetime.time(23, 0)
        maintenance_stop_time = datetime.time(5, 0)
        return is_time_between(now.time(), maintenance_start_time, maintenance_stop_time)
