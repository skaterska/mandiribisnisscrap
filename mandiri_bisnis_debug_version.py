from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from collections import namedtuple

# load an expensive lib
import numpy as np
import numpy.core.defchararray as np_f
import pandas as pd

import time

from sqlalchemy.orm import sessionmaker
from database_manager import (
    engine, NotifMutasi
)

# init db
Session = sessionmaker(bind=engine)
session = Session()


chrome_options = webdriver.ChromeOptions()
# chrome_options.add_argument('headless')
chrome_options.add_argument('window-size=1200x600')
chrome_options.add_argument('--disable-notifications')
chrome_options.add_argument("--start-maximized")
chrome_options.add_argument('log-level=3')

driver = webdriver.Chrome("G:\\master\\chromedriver\\2.45\\chromedriver.exe", chrome_options=chrome_options)
URL = "file:///G:/work/projects.co.id/scrap_mutasi_bank/mandiri/depan_mandiri_bisnis.html"
COMPANY_ID = 'FD07558'
USERNAME = 'EKLAN2'
PASSWORD = 'Eklan246'

# debug dev
URL_HOME = "file:///G:/work/projects.co.id/scrap_mutasi_bank/mandiri/mandiri_home.html"
URL_MUTASI_TAMPILKAN = "file:///G:/work/projects.co.id/scrap_mutasi_bank/mandiri/mutasi_rekening.html"
URL_AKHIR_MUTASI = "file:///G:/work/projects.co.id/scrap_mutasi_bank/mandiri/eksekusi_mutasi.html"
URL_MUTASI_SEBELUMNYA = "file:///G:/work/projects.co.id/scrap_mutasi_bank/mandiri/mutasi_rekening_halaman_akhir.html"
URL_HAL_MUTASI_SEBELUMNYA = "file:///G:/work/projects.co.id/scrap_mutasi_bank/mandiri/" \
                            "mutasi_rekening_halaman_sebelum_akhir.html"


def site_login():
    driver.get(URL)
    driver.find_element_by_name('corpId').send_keys(COMPANY_ID)
    driver.find_element_by_name('userName').send_keys(USERNAME)
    driver.find_element_by_name('passwordEncryption').send_keys(PASSWORD)
    driver.find_element_by_xpath('//*[@id="button"]').click()


def klik_mutasi():
    driver.get(URL_HOME)
    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "menuFrame")))
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="masterdiv"]/div[2]')
    )).click()
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="subs9"]')
    )).click()


def klik_tampilkan_mutasi():
    driver.get(URL_MUTASI_TAMPILKAN)
    # WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "mainFrame")))
    driver.switch_to.default_content()
    driver.switch_to.frame('mainFrame')
    # WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
    #     (By.NAME, 'show1')
    # )).click()
    # driver.switch_to.frame(driver.find_elements_by_xpath('/html/frameset/frameset/frame[2]'))
    time.sleep(10)
    print("Going to klik...")
    driver.find_element_by_name('show1').click()


def klik_akhir_mutasi():
    driver.get(URL_AKHIR_MUTASI)
    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "mainFrame")))
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
        (By.XPATH, '/html/body/form/table[4]/tbody/tr[27]/td/a[2]')
    )).click()


def klik_mutasi_sebelumnya():
    driver.get(URL_MUTASI_SEBELUMNYA)
    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "mainFrame")))
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
        (By.XPATH, '/html/body/form/table[4]/tbody/tr[5]/td/a[2]')
    )).click()
    driver.find_element_by_name('show1').click()


def download_mutasi_hal_akhir():
    driver.get(URL_MUTASI_SEBELUMNYA)
    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "mainFrame")))
    return driver.find_elements_by_xpath('/html/body/form/table[4]/tbody/tr')
    # return driver.find_element_by_class_name('clsFormTrxStatus')


def download_mutasi_seb_hal_akhir():
    driver.get(URL_HAL_MUTASI_SEBELUMNYA)
    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "mainFrame")))
    return driver.find_elements_by_xpath('/html/body/form/table[4]/tbody/tr')
    # return driver.find_element_by_class_name('clsFormTrxStatus')


def extract_table_to_data(table: list) -> np.array:
    f_data = []
    for row in table:
        for cell in row.find_elements_by_tag_name('td'):
            f_data.append(cell.text)
    data_cell = np.array(f_data[6:-11])
    data_cell.shape = int(len(data_cell)/6), 6
    return np.array(f_data[:6]), data_cell


def data_mutasi():
    raw_data_hal_akhir = download_mutasi_hal_akhir()
    header, data_text_hal_akhir = extract_table_to_data(raw_data_hal_akhir)
    raw_data_hal_seb_akhir = download_mutasi_seb_hal_akhir()
    _, data_text_seb_hal_akhir = extract_table_to_data(raw_data_hal_seb_akhir)
    return header, np.vstack([data_text_seb_hal_akhir, data_text_hal_akhir])


def clean_data(f_data):
    # set str to timedate
    f_data[:, 0] = pd.to_datetime(f_data[:, 0])
    f_data[:, 1] = pd.to_datetime(f_data[:, 1])

    # debit
    f_data[:, 3] = np_f.replace(f_data[:, 3], ',', '')
    # f_data[:, 3].astype(np.float)

    # credit
    f_data[:, 4] = np_f.replace(f_data[:, 4], ',', '')
    # f_data[:, 4].astype(np.float)

    DataMutasi = namedtuple('DataMutasi', 'datetime valdate desc debit kredit saldo')
    return [DataMutasi(*x) for x in f_data]


def save_to_sql(f_data):
    for row in f_data:
        session.add(
            NotifMutasi(
                datetime=row.datetime,
                valdate=row.valdate,
                desc=row.desc,
                debit=row.debit,
                kredit=row.kredit,
                saldo=row.saldo,
            )
        )
        session.commit()


def close():
    driver.close()
    driver.quit()


while True:
    # site_login()
    # klik_mutasi()
    klik_tampilkan_mutasi()
    time.sleep(20)
# klik_akhir_mutasi()
# klik_mutasi_sebelumnya()

# header_table, data = data_mutasi()
# time.sleep(2)
# _, new_data = data_mutasi()
# diff = len(new_data) - len(data)
# if data.shape != new_data.shape:
#     # data less than 24
#     if diff > 0:
#         target = new_data[-diff:]
#         save_to_sql(target)
#     else:
#         target = None
# else:
#     if diff > 0:
#         index = np.where(np.isin(new_data, data[-1]))
#         target = new_data[index[0][0]:]
#         save_to_sql(target)
#     else:
#         target = None

# ready_to_sql = clean_data(target)
# time.sleep(5)
# close()
