from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.common.exceptions import (
    NoSuchElementException,
    TimeoutException,
    NoSuchFrameException,
    UnexpectedAlertPresentException,
    NoAlertPresentException
)
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from collections import namedtuple

# load an expensive lib
import numpy as np
import numpy.core.defchararray as np_f
import pandas as pd

import datetime
import time
from copy import deepcopy


from sqlalchemy.orm import sessionmaker
from database_manager import (
    engine, NotifMutasi
)

from utils import (
    is_connected,
    is_db_maintenance
    # go_beep,
)
# from pyvirtualdisplay import Display

# init db
Session = sessionmaker(bind=engine)
session = Session()

# init screen virtually
# display = Display(visible=0, size=(1366, 800))
# display.start()

# user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 ' \
#              '(KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
# user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 ' \
#              '(KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36'
chrome_options = webdriver.ChromeOptions()

# chrome_options.add_argument('user-agent={}'.format(user_agent))
# chrome_options.add_argument("--headless")
chrome_options.add_argument('window-size=1200x600')
chrome_options.add_argument('--disable-notifications')
chrome_options.add_argument("--start-maximized")
chrome_options.add_argument('log-level=3')

capabilities = DesiredCapabilities.CHROME.copy()
capabilities['acceptSslCerts'] = True
capabilities['acceptInsecureCerts'] = True

driver = webdriver.Chrome("C:\\driver\\chromedriver.exe",
                          chrome_options=chrome_options,
                          desired_capabilities=capabilities,
                          )
URL = "https://mib.bankmandiri.co.id/sme/common/login.do?action=logoutSME"
COMPANY_ID = 'FD07558'
USERNAME = 'EKLAN2'
PASSWORD = 'Eklan246'

# debug dev
URL_HOME = "file:///G:/work/projects.co.id/scrap_mutasi_bank/mandiri/mandiri_home.html"
URL_MUTASI_TAMPILKAN = "file:///G:/work/projects.co.id/scrap_mutasi_bank/mandiri/mutasi_rekening.html"
URL_AKHIR_MUTASI = "file:///G:/work/projects.co.id/scrap_mutasi_bank/mandiri/eksekusi_mutasi.html"
URL_MUTASI_SEBELUMNYA = "file:///G:/work/projects.co.id/scrap_mutasi_bank/mandiri/mutasi_rekening_halaman_akhir.html"
URL_HAL_MUTASI_SEBELUMNYA = "file:///G:/work/projects.co.id/scrap_mutasi_bank/mandiri/" \
                            "mutasi_rekening_halaman_sebelum_akhir.html"


def error_routine_table():
    if is_connected():
        print("Seems data only one page, go around..")
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    else:
        print("no internet will try again in 60 secs...")
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        time.sleep(60)


def handling_error_db_boring():
    try:
        if is_db_maintenance():
            raise NoSuchElementException
        else:
            driver.switch_to.default_content()
            WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "mainFrame")))
            WebDriverWait(driver, 60).until(EC.visibility_of_element_located(
                (By.CLASS_NAME, 'clsFormTrxStatus')
            ))

    except NoSuchElementException:
        print("database seems in maintenance..")
        print("will be try again in logout and login again in 5 minutes..")
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        time.sleep(5)
        logout()
        time.sleep(300)
        if is_db_maintenance():
            handling_error_db_boring()
        else:
            site_login()
            time.sleep(3)
            klik_mutasi_pertama()
            time.sleep(3)
            klik_tampilkan_mutasi()

    except TimeoutException:
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        print("Timeout exceptions...")
        print("database seems in maintenance..")
        print("will be try again in logout and login again in 5 minutes..")
        time.sleep(5)
        logout()
        time.sleep(300)
        site_login()
        time.sleep(3)
        klik_mutasi_pertama()
        time.sleep(3)
        klik_tampilkan_mutasi()


def site_login():
    driver.get(URL)
    try:
        driver.find_element_by_name('corpId').send_keys(COMPANY_ID)
        driver.find_element_by_name('userName').send_keys(USERNAME)
        driver.find_element_by_name('passwordEncryption').send_keys(PASSWORD)
        driver.find_element_by_xpath('//*[@id="button"]').click()

        try:
            driver.switch_to.default_content()
            WebDriverWait(driver, 60).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "menuFrame")))
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located(
                (By.LINK_TEXT, 'Informasi Rekening')
            ))
        except UnexpectedAlertPresentException:
            print("LOGIN in used... will try again in 5 minutes..")
            try:
                driver.switch_to.alert.accept()
            except NoAlertPresentException:
                pass
            time.sleep(300)
            site_login()

    except NoSuchElementException:
        if is_connected():
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            print("Access denied by Mandiri...")
            print("Programm will shutdown..")
            time.sleep(3)
            logout()
            close()
        else:
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            print("no internet connections")
            close()

    except NoSuchFrameException:
        if is_connected():
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            print("Access denied by Mandiri...")
            print("Programm will shutdown..")
            time.sleep(3)
            logout()
        else:
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            print("no internet connections")


def klik_mutasi_pertama():
    # driver.get(URL_HOME)
    driver.switch_to.default_content()
    WebDriverWait(driver, 60).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "menuFrame")))
    try:
        WebDriverWait(driver, 60).until(EC.element_to_be_clickable(
            (By.LINK_TEXT, 'Informasi Rekening')
        )).click()
        WebDriverWait(driver, 60).until(EC.element_to_be_clickable(
            (By.XPATH, '//*[@id="subs9"]')
        )).click()
    except NoSuchElementException:
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        print("May be login in used..")
        print("Logout..")
        logout()


def klik_mutasi_kontinyu():
    # driver.get(URL_HOME)
    driver.switch_to.default_content()
    WebDriverWait(driver, 60).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "menuFrame")))
    # WebDriverWait(driver, 60).until(EC.element_to_be_clickable(
    #     (By.LINK_TEXT, 'Informasi Rekening')
    # )).click()
    try:
        WebDriverWait(driver, 60).until(EC.element_to_be_clickable(
            (By.XPATH, '//*[@id="subs9"]')
        )).click()
    except TimeoutException:
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        print("timeout klik mutasi")
    except NoSuchElementException:
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        print("No element")


def klik_tampilkan_mutasi():
    try_to_connect = 0
    time.sleep(3)
    # driver.get(URL_MUTASI_TAMPILKAN)
    # WebDriverWait(driver, 60).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "mainFrame")))
    driver.switch_to.default_content()
    driver.switch_to.frame('mainFrame')
    time.sleep(1)
    print("going to klik 'tampilkan'.. ")
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    time.sleep(3)

    try:
        WebDriverWait(driver, 60).until(EC.element_to_be_clickable(
            (By.NAME, 'show1')
        )).click()
        # driver.find_element_by_name('show1').click()
        handling_error_db_boring()
    except NoSuchElementException:
        if is_connected():
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            print("database seems in maintenance..")
            print("will be try again in 5 minutes..")
            time.sleep(300)
            klik_tampilkan_mutasi()
        else:
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            print("no connections")
            try_to_connect += 1
            if try_to_connect > 2:
                print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                print("We have try to connect 3 times but still no internet, so.. program abort")
                logout()
                close()
            else:
                print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                print("No connection..")
                print("will be try again in 5 minutes..")
                time.sleep(300)
                klik_tampilkan_mutasi()

    except TimeoutException:
        if is_connected():
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            print("database seems in maintenance..")
            print("will be try again in 5 minutes..")
            time.sleep(300)
            klik_tampilkan_mutasi()
        else:
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            print("no connections")
            logout()
            close()


def klik_akhir_mutasi():
    # driver.get(URL_AKHIR_MUTASI)
    driver.switch_to.default_content()
    print("heading to last page transaction..")
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    time.sleep(3)
    driver.switch_to.frame('mainFrame')
    try:
        # WebDriverWait(driver, 60).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "mainFrame")))
        WebDriverWait(driver, 60).until(EC.element_to_be_clickable(
            (By.LINK_TEXT, 'Akhir')
        )).click()
        # driver.find_element_by_link_text('Akhir').click()

    except NoSuchElementException:
        if is_connected():
            print("Seems data only one page, go around..")
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        else:
            print("no internet will try again in 60 secs...")
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            time.sleep(60)
            klik_akhir_mutasi()

    except TimeoutException:
        if is_connected():
            print("Seems data only one page, go around..")
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        else:
            print("no internet will try again in 60 secs...")
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            time.sleep(60)
            klik_akhir_mutasi()


def klik_mutasi_sebelumnya():
    # driver.get(URL_MUTASI_SEBELUMNYA)
    driver.switch_to.default_content()
    time.sleep(3)
    print("heading to (last page - 1) transactions")
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    driver.switch_to.frame('mainFrame')
    try:
        # WebDriverWait(driver, 60).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "mainFrame")))
        WebDriverWait(driver, 60).until(EC.element_to_be_clickable(
            (By.LINK_TEXT, 'Sebelumnya')
        )).click()
        # driver.find_element_by_link_text('Sebelumnya').click()
    except NoSuchElementException:
        print("Seems data only one page, go around..")
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    except TimeoutException:
        print("Seems data only one page, go around..")
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


def download_mutasi_hal_akhir():
    # driver.get(URL_MUTASI_SEBELUMNYA)
    driver.switch_to.default_content()
    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "mainFrame")))
    return driver.find_elements_by_xpath('/html/body/form/table[4]/tbody/tr')
    # return driver.find_element_by_class_name('clsFormTrxStatus')


def download_mutasi_seb_hal_akhir():
    # driver.get(URL_HAL_MUTASI_SEBELUMNYA)
    driver.switch_to.default_content()
    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.NAME, "mainFrame")))
    return driver.find_elements_by_xpath('/html/body/form/table[4]/tbody/tr')
    # return driver.find_element_by_class_name('clsFormTrxStatus')


def extract_table_to_data(table: list) -> np.array:
    f_data = []
    for row in table:
        for cell in row.find_elements_by_tag_name('td'):
            f_data.append(cell.text)
    data_cell = np.array(f_data[6:-11])
    if len(data_cell) % 2:
        # when data not eligible to divide 6 add
        # plus one empty data to the 'data_cell'
       data_cell = np.array(f_data[6:-10])

    data_cell.shape = int(len(data_cell)/6), 6
    return np.array(f_data[:6]), data_cell


def data_mutasi():
    klik_akhir_mutasi()
    raw_data_hal_akhir = download_mutasi_hal_akhir()
    header, data_text_hal_akhir = extract_table_to_data(raw_data_hal_akhir)
    klik_mutasi_sebelumnya()
    raw_data_hal_seb_akhir = download_mutasi_seb_hal_akhir()
    _, data_text_seb_hal_akhir = extract_table_to_data(raw_data_hal_seb_akhir)
    return header, np.vstack([data_text_seb_hal_akhir, data_text_hal_akhir])


def clean_data(f_data):
    # set str to timedate
    f_data[:, 0] = pd.to_datetime(f_data[:, 0])
    f_data[:, 1] = pd.to_datetime(f_data[:, 1])

    # debit
    f_data[:, 3] = np_f.replace(f_data[:, 3], ',', '')
    # f_data[:, 3].astype(np.float)

    # credit
    f_data[:, 4] = np_f.replace(f_data[:, 4], ',', '')
    # f_data[:, 4].astype(np.float)

    DataMutasi = namedtuple('DataMutasi', 'datetime valdate desc debit kredit saldo')
    return [DataMutasi(*x) for x in f_data]


def save_to_sql(f_data):
    for row in f_data:
        session.add(
            NotifMutasi(
                datetime=row.datetime,
                valdate=row.valdate,
                desc=row.desc,
                debit=row.debit,
                kredit=row.kredit,
                saldo=row.saldo,
            )
        )
        session.commit()


def logout():
    try:
        driver.switch_to.default_content()
        driver.switch_to.frame('topFrame')
        driver.find_element_by_class_name('button_out').click()

    except NoSuchElementException:
        print("Failed Logout going shutdown...")
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        time.sleep(3)

    except NoSuchFrameException:
        print("No frame to normally logout..")
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


def close():
    driver.close()
    driver.quit()


# Begin Scrapping initiate
site_login()
klik_mutasi_pertama()
time.sleep(3)
klik_tampilkan_mutasi()
time.sleep(3)
header_table, data = data_mutasi()
time.sleep(3)

while True:
    klik_mutasi_kontinyu()
    time.sleep(3)
    klik_tampilkan_mutasi()
    time.sleep(3)
    _, new_data = data_mutasi()
    diff = len(new_data) - len(data)
    if data.shape != new_data.shape:
        # data less than 24
        if diff > 0:
            # go_beep()
            print("we have new data...")
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            target = new_data[-diff:]
            clean_target = clean_data(target)
            save_to_sql(clean_target)
        else:
            target = None
    else:
        if diff > 0:
            print("we have many new data...")
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            # go_beep()
            # go_beep()
            index = np.where(np.isin(new_data, data[-1]))
            target = new_data[index[0][0]:]
            clean_target = clean_data(target)
            save_to_sql(clean_target)
        else:
            target = None

    data = deepcopy(new_data)
    # end while
# end
