# scrap_mandiri
script kebutuhan personal untuk melakukan pengumpulan data dan akun bank Mandiri Bisnis.

Skrip ini ditulis diatas bahasa python versi :
python-3.6.8

buat akun mysql 'mandiri_bisnis':'mandiri_bisnis', dan database 'mandiri_bisnis'
buat venv python, kemudian jalankan 'database_manager.py' untuk :

<code>python database_manager.py</code>
perintah diatas untuk membuat struktur tabel database mandiri_bisnis pada 

dan library-library berikut:

<code>mysqlclient==1.4.1 <br>
numpy==1.16.0 <br>
pandas==0.24.0 <br>
python-dateutil==2.7.5 <br>
pytz==2018.9 <br>
selenium==3.141.0 <br>
six==1.12.0 <br>
SQLAlchemy==1.2.16 <br>
urllib3==1.24.1 <br>
</code>