import sqlalchemy

from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

url = 'mysql://mandiri_bisnis:mandiri_bisnis@localhost/mandiri_bisnis'
engine = sqlalchemy.create_engine(url)


class NotifMutasi(Base):
    __tablename__ = 'notifmutasi'
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    datetime = sqlalchemy.Column(sqlalchemy.DATETIME)
    valdate = sqlalchemy.Column(sqlalchemy.DATE)
    desc = sqlalchemy.Column(sqlalchemy.String(250), nullable=True)
    debit = sqlalchemy.Column(sqlalchemy.String(30))
    kredit = sqlalchemy.Column(sqlalchemy.String(30))
    saldo = sqlalchemy.Column(sqlalchemy.String(30), nullable=True)


Base.metadata.create_all(engine)
